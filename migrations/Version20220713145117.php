<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220713145117 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE catastrophe (id INT AUTO_INCREMENT NOT NULL, catastrophe_id INT NOT NULL, continent_id INT NOT NULL, categorie_id INT NOT NULL, souscategorie_id INT NOT NULL, pays_id INT NOT NULL, localisation VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, nombre_mort INT NOT NULL, nombre_blesses INT NOT NULL, ville VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', image_url VARCHAR(255) DEFAULT NULL, nom_catastrophe VARCHAR(255) NOT NULL, autres_victimes INT DEFAULT NULL, sans_abris INT DEFAULT NULL, dimension DOUBLE PRECISION DEFAULT NULL, cause_catastrophe VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, longitude VARCHAR(255) DEFAULT NULL, latitude VARCHAR(255) DEFAULT NULL, INDEX IDX_44F3D1D450560F60 (catastrophe_id), INDEX IDX_44F3D1D4921F4C77 (continent_id), INDEX IDX_44F3D1D4BCF5E72D (categorie_id), INDEX IDX_44F3D1D4A27126E0 (souscategorie_id), INDEX IDX_44F3D1D4A6E44244 (pays_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE continent (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE images (id INT AUTO_INCREMENT NOT NULL, images_select_id INT NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_E01FBE6AAD087056 (images_select_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pays (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE souscategorie (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, image_user VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE catastrophe ADD CONSTRAINT FK_44F3D1D450560F60 FOREIGN KEY (catastrophe_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE catastrophe ADD CONSTRAINT FK_44F3D1D4921F4C77 FOREIGN KEY (continent_id) REFERENCES continent (id)');
        $this->addSql('ALTER TABLE catastrophe ADD CONSTRAINT FK_44F3D1D4BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE catastrophe ADD CONSTRAINT FK_44F3D1D4A27126E0 FOREIGN KEY (souscategorie_id) REFERENCES souscategorie (id)');
        $this->addSql('ALTER TABLE catastrophe ADD CONSTRAINT FK_44F3D1D4A6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6AAD087056 FOREIGN KEY (images_select_id) REFERENCES catastrophe (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6AAD087056');
        $this->addSql('ALTER TABLE catastrophe DROP FOREIGN KEY FK_44F3D1D4BCF5E72D');
        $this->addSql('ALTER TABLE catastrophe DROP FOREIGN KEY FK_44F3D1D4921F4C77');
        $this->addSql('ALTER TABLE catastrophe DROP FOREIGN KEY FK_44F3D1D4A6E44244');
        $this->addSql('ALTER TABLE catastrophe DROP FOREIGN KEY FK_44F3D1D4A27126E0');
        $this->addSql('ALTER TABLE catastrophe DROP FOREIGN KEY FK_44F3D1D450560F60');
        $this->addSql('DROP TABLE catastrophe');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE continent');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE pays');
        $this->addSql('DROP TABLE souscategorie');
        $this->addSql('DROP TABLE `user`');
    }
}
