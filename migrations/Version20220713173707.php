<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220713173707 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE catastrophe DROP FOREIGN KEY FK_44F3D1D450560F60');
        $this->addSql('DROP INDEX IDX_44F3D1D450560F60 ON catastrophe');
        $this->addSql('ALTER TABLE catastrophe DROP catastrophe_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE catastrophe ADD catastrophe_id INT NOT NULL');
        $this->addSql('ALTER TABLE catastrophe ADD CONSTRAINT FK_44F3D1D450560F60 FOREIGN KEY (catastrophe_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_44F3D1D450560F60 ON catastrophe (catastrophe_id)');
    }
}
