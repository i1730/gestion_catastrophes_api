<?php

namespace App\Controller;

use App\Repository\CatastropheRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="app_article")
     */
    public function index(CatastropheRepository $catastropheRepository): Response
    {
        $catastrophes = $catastropheRepository->findByPays();
        dd($catastrophes);
        return $this->render('article/index.html.twig', [
            '$catastrophes' => $catastrophes
        ]);
    }
}
