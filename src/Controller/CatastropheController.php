<?php

namespace App\Controller;

use App\Entity\Catastrophe;
use App\Form\CatastropheType;
use App\Repository\CatastropheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catastrophe")
 */
class CatastropheController extends AbstractController
{
    /**
     * @Route("/", name="app_catastrophe_index", methods={"GET", "POST", "PUT", "DELETE"})
     */
    public function index(CatastropheRepository $catastropheRepository): Response
    {
        return $this->render('catastrophe/index.html.twig', [
            'catastrophes' => $catastropheRepository->findBy(array(), array('createdAt' => 'DESC')),
        ]);
        
    }

    /**
     * @Route("/new", name="app_catastrophe_new", methods={"GET", "POST"})
     */
    public function new(Request $request, CatastropheRepository $catastropheRepository, EntityManagerInterface $manager): Response
    {
        $catastrophe = new Catastrophe();
        $form = $this->createForm(CatastropheType::class, $catastrophe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($catastrophe);
            // dd($catastrophe);
            $manager->flush();
            // $catastropheRepository->add($catastrophe);
            // dd($catastropheRepository);
            return $this->redirectToRoute('app_catastrophe_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('catastrophe/new.html.twig', [
            'catastrophe' => $catastrophe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_catastrophe_show", methods={"GET"})
     */
    public function show(Catastrophe $catastrophe): Response
    {
        return $this->render('catastrophe/show.html.twig', [
            'catastrophe' => $catastrophe,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_catastrophe_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Catastrophe $catastrophe, CatastropheRepository $catastropheRepository, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CatastropheType::class, $catastrophe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $catastropheRepository->add($catastrophe);
            $entityManager->persist($catastrophe);
            $entityManager->flush();
            return $this->redirectToRoute('app_catastrophe_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('catastrophe/edit.html.twig', [
            'catastrophe' => $catastrophe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_catastrophe_delete", methods={"POST"})
     */
    public function delete(Request $request, Catastrophe $catastrophe, CatastropheRepository $catastropheRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$catastrophe->getId(), $request->request->get('_token'))) {
            $catastropheRepository->remove($catastrophe);
        }

        return $this->redirectToRoute('app_catastrophe_index', [], Response::HTTP_SEE_OTHER);
    }
}
