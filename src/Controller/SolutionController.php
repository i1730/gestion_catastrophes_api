<?php

namespace App\Controller;

use App\Entity\Solution;
use App\Form\SolutionType;
use App\Repository\SolutionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/solution")
 */
class SolutionController extends AbstractController
{
    /**
     * @Route("/", name="app_solution_index", methods={"GET"})
     */
    public function index(SolutionRepository $solutionRepository): Response
    {
        return $this->render('solution/index.html.twig', [
            'solutions' => $solutionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_solution_new", methods={"GET", "POST"})
     */
    public function new(Request $request, SolutionRepository $solutionRepository): Response
    {
        $solution = new Solution();
        $form = $this->createForm(SolutionType::class, $solution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $solutionRepository->add($solution);
            return $this->redirectToRoute('app_solution_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('solution/new.html.twig', [
            'solution' => $solution,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_solution_show", methods={"GET"})
     */
    public function show(Solution $solution): Response
    {
        return $this->render('solution/show.html.twig', [
            'solution' => $solution,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_solution_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Solution $solution, SolutionRepository $solutionRepository): Response
    {
        $form = $this->createForm(SolutionType::class, $solution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $solutionRepository->add($solution);
            return $this->redirectToRoute('app_solution_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('solution/edit.html.twig', [
            'solution' => $solution,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_solution_delete", methods={"POST"})
     */
    public function delete(Request $request, Solution $solution, SolutionRepository $solutionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$solution->getId(), $request->request->get('_token'))) {
            $solutionRepository->remove($solution);
        }

        return $this->redirectToRoute('app_solution_index', [], Response::HTTP_SEE_OTHER);
    }
}
