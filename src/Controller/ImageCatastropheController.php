<?php

namespace App\Controller;

use App\Entity\Catastrophe;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ImageCatastropheController extends AbstractController
{
    public function __invoke(Request $request):Catastrophe
    {
        // $catastrophe = $request->attributes->get('data');
        // $file = $request->files->get('imageFile');
        // $catastrophe ->set($request->files->get('file'));
        // $post -> setUpdateAt(new \DateTime());
        // return $post;
        // dd($file, $post);

        // $catastrophe = $request->attributes->get('data');
        // $file = $request->files->get('file');
        // $post ->setFile($request->files->get('file'));
        // $post -> setUpdateAt(new \DateTime());
        // return $post;
        // dd($file, $post);
        
        $uploadFile = $request->files->get('imageFile');
        if (! $uploadFile) {
            throw new BadRequestHttpException('"file" is required');
        }
        $mediaObject = new Catastrophe();
        $mediaObject->imageFile= $uploadFile;
        return $mediaObject;
    }
}
