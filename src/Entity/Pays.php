<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PaysRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *  @ApiResource(
 * 
 *    collectionOperations={
 *       "get",
 *       "post"
 *    },
 *    normalizationContext={"groups"={"article:read"}},
 *    denormalizationContext={"groups"={"article:write"}}
 * 
 * )
 * @ORM\Entity(repositoryClass=PaysRepository::class)
 */
class Pays
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("article:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("article:read")
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Catastrophe::class, mappedBy="pays")
     */
    private $paysSelect;


    public function __construct()
    {
        $this->paysSelect = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Catastrophe>
     */
    public function getPaysSelect(): Collection
    {
        return $this->paysSelect;
    }

    public function addPaysSelect(Catastrophe $paysSelect): self
    {
        if (!$this->paysSelect->contains($paysSelect)) {
            $this->paysSelect[] = $paysSelect;
            $paysSelect->setPays($this);
        }

        return $this;
    }

    public function removePaysSelect(Catastrophe $paysSelect): self
    {
        if ($this->paysSelect->removeElement($paysSelect)) {
            // set the owning side to null (unless already changed)
            if ($paysSelect->getPays() === $this) {
                $paysSelect->setPays(null);
            }
        }

        return $this;
    }

  

    
    public function __toString()
    {
        return $this->nom;
    }
}
