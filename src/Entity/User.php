<?php

namespace App\Entity;

use DateTimeImmutable;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ApiResource(
 * 
 *    itemOperations={
 * 
 *    "get"={
 *             "access_control"="is_granted('IS_AUTHENTICATED_FULLY')",
 *             "normalization_context"={ 
 *                   "groups"={"get"}   
 *                   }
 *       },
 * 
 *   },
 *    collectionOperations={"post"},
 *    normalizationContext={
 *       "groups"={"user:read"} 
 *      }
 * 
 *   )
 * @UniqueEntity(fields={"email"},
 * message= "L'email que vous avez indiquez est déja utilisé !"
 * )
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read"})
     * @Assert\NotBlank()
     * @Assert\Length(min=4, max=255)
     */
    private $username;

        /**
     * @var File
     * @Vich\UploadableField(mapping="blog_images", fileNameProperty="imageUrl")
     */
    private $imageFile;

    // /**
    //  * @ORM\OneToMany(targetEntity=Catastrophe::class, mappedBy="catastrophe")
    //  * @Groups({"article:read", "post"})
    //  */
    // private $catastrophes;

    /**
     * @Groups({"user:read"})
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8",minMessage="Votre mot de passe doit faire minimum 8 caractères")
     * @Assert\EqualTo(propertyPath="confirm_password",message="Votre mot de passe doit être le  même  ")
     * 
     */
    private $password;

     /**
     * @Assert\EqualTo(propertyPath="password",message="Votre mot de passe doit être le  même  ")
     * 
     */
    public  $confirm_password;
     /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read"})
     */
    private $slug;
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * @Groups({"user:read"})
     */
    private $imageUser;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups({"read"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->setcreatedAt(new \DateTimeImmutable);
    }

     /**
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate 
     * @return void 
     */
    public function initializeSlug()
    {
        if (empty($this->slug)) {
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->email);
        }
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
        
    }

    public function getSalt()
    {
        return null;
    }

    public  function eraseCredentials()
    {
        
    }

         /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
        
    }
    

  

    /**
     * Get the value of slug
     */ 
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of slug
     *
     * @return  self
     */ 
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
   
    /**
     * Get the value of imageUser
     */ 
    public function getImageUser()
    {
        return $this->imageUser;
    }

    /**
     * Set the value of imageUser
     *
     * @return  self
     */ 
    public function setImageUser($imageUser)
    {
        $this->imageUser = $imageUser;

        return $this;
    }
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
      /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageUser = null): void
    {
        $this->imageFile = $imageUser;

        if (null !== $imageUser) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setcreatedAt(new \DateTimeImmutable);
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
}