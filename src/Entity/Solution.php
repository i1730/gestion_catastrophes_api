<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SolutionRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SolutionRepository::class)
 */
class Solution
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("article:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $nomCatastrophe;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $message;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups({"article:read", "post"})
     */
    private $date;

    public function __construct()
    {
        $this->date = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of nomCatastrophe
     */ 
    public function getNomCatastrophe()
    {
        return $this->nomCatastrophe;
    }

    /**
     * Set the value of nomCatastrophe
     *
     * @return  self
     */ 
    public function setNomCatastrophe($nomCatastrophe)
    {
        $this->nomCatastrophe = $nomCatastrophe;

        return $this;
    }
}
