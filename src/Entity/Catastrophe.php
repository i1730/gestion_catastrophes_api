<?php

namespace App\Entity;

use DateTimeImmutable;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CatastropheRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ImageCatastropheController;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;


/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ApiResource(
 * attributes ={"order" = {"createdAt": "DESC"}} ,
 * 
 *   collectionOperations={
 *       "post" = {
 *          "controller" = ImageCatastropheController::class,
 * "deserialize" = false,
 * "openapi-context"={
 *      "requestBody"={
 *  "content"={
 * "multipart/form-data"={
 * "shema"={
 * "type"="object",
 * "properties"={
 * "file"={
 * "type"="string",
 * "format"="binary"
 * }
 * }
 * }
 * }
 * }
 * }
 *  }
 *          },
 *       "get",
 *    },
 *    normalizationContext={"groups"={"article:read"}},
 *    denormalizationContext={"groups"={"post"}}
 * 
 * 
 *    )
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=CatastropheRepository::class)
 */
class Catastrophe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("article:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $localisation;

    /**
     * @ORM\Column(type="text")
     * @Groups({"article:read", "post"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"article:read", "post"})
     */
    private $nombreMort;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"article:read", "post"})
     */
    private $nombreBlesses;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $ville;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups({"article:read", "post"})
     */
    private $createdAt;

    /**
     * @var File
     * @Vich\UploadableField(mapping="blog_images", fileNameProperty="imageUrl")
     * @Groups({"article:create", "post"})
     */
    private $imageFile;

    /**
     * 
     *@ORM\Column(type="string", length=255, nullable = true)
     * @Groups({"article:read", "post"})
     * 
     */
    private $imageUrl;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $nomCatastrophe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"article:read", "post"})
     */
    private $autresVictimes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"article:read", "post"})
     */
    private $sansAbris;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"article:read", "post"})
     */
    private $dimension;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $causeCatastrophe;

    // /**
    //  * @ORM\ManyToOne(targetEntity=User::class, inversedBy="catastrophes")
    //  * @ORM\JoinColumn(nullable=false)
    //  * @Groups({"article:read", "post"})
    //  */
    // private $catastrophe;

    /**
     * @ORM\ManyToOne(targetEntity=Continent::class, inversedBy="continentSelect")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article:read", "post"})
     */
    private $continent;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="categorieSelect")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article:read", "post"})
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity=Souscategorie::class, inversedBy="souscategorieSelect")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article:read", "post"})
     */
    private $souscategorie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity=Pays::class, inversedBy="paysSelect")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article:read", "post"})
     */
    private $pays;

    /**
     * @ORM\OneToMany(targetEntity=Images::class, mappedBy="imagesSelect",cascade={"persist"})
     * @Groups({"article:read", "post"})
     */
    private $images;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"article:read", "post"})
     */
    private $longitude;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article:read", "post"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"article:read", "post"})
     */
    private $latitude;

    /**
     * 
     * @ORM\PrePersist
     * @ORM\PreUpdate 
     * @return void 
     */
    public function initializeSlug()
    {
        if (empty($this->slug)) {
            $slugify = new Slugify();
            $this->slug = $slugify->slugify($this->nomCatastrophe);
        }
    }

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNombreMort(): ?int
    {
        return $this->nombreMort;
    }

    public function setNombreMort(int $nombreMort): self
    {
        $this->nombreMort = $nombreMort;

        return $this;
    }

    public function getNombreBlesses(): ?int
    {
        return $this->nombreBlesses;
    }

    public function setNombreBlesses(int $nombreBlesses): self
    {
        $this->nombreBlesses = $nombreBlesses;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getNomCatastrophe(): ?string
    {
        return $this->nomCatastrophe;
    }

    public function setNomCatastrophe(string $nomCatastrophe): self
    {
        $this->nomCatastrophe = $nomCatastrophe;

        return $this;
    }

    public function getAutresVictimes(): ?int
    {
        return $this->autresVictimes;
    }

    public function setAutresVictimes(?int $autresVictimes): self
    {
        $this->autresVictimes = $autresVictimes;

        return $this;
    }

    public function getSansAbris(): ?int
    {
        return $this->sansAbris;
    }

    public function setSansAbris(?int $sansAbris): self
    {
        $this->sansAbris = $sansAbris;

        return $this;
    }

    public function getDimension(): ?float
    {
        return $this->dimension;
    }

    public function setDimension(?float $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getCauseCatastrophe(): ?string
    {
        return $this->causeCatastrophe;
    }

    public function setCauseCatastrophe(string $causeCatastrophe): self
    {
        $this->causeCatastrophe = $causeCatastrophe;

        return $this;
    }


    public function getContinent(): ?Continent
    {
        return $this->continent;
    }

    public function setContinent(?Continent $continent): self
    {
        $this->continent = $continent;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getSouscategorie(): ?Souscategorie
    {
        return $this->souscategorie;
    }

    public function setSouscategorie(?Souscategorie $souscategorie): self
    {
        $this->souscategorie = $souscategorie;

        return $this;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * @return Collection<int, Images>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Images $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setImagesSelect($this);
        }

        return $this;
    }

    public function removeImage(Images $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getImagesSelect() === $this) {
                $image->setImagesSelect(null);
            }
        }

        return $this;
    }
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }
    /**
     * Get the value of username
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }



    public function getCatastrophe(): ?User
    {
        return $this->catastrophe;
    }

    public function setCatastrophe(?User $catastrophe): self
    {
        $this->catastrophe = $catastrophe;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->createdAt = new \DateTimeImmutable();
        }
    }
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
    /**
     * Get the value of imageUrl
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * Set the value of imageUrl
     *
     * @return  self
     */
    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;

        // return $this;
    }
}
